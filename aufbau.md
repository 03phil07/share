# Flyer Arbeitsschutz

## PHIL

### Gesetze und Verordnungen

Die Arbeitsstättenverordnung(Arbschg)  regelt die Sicherheit und den Schutz die der arbeitgeber gewährleisten muss.

Der Arbeitgeber ist gemäß § 3 ArbSchG verpflichtet, „eine Verbesserung von Sicherheit und Gesundheitsschutz der Beschäftigten anzustreben”.

„(1) Diese Verordnung dient der Sicherheit und dem Schutz der Gesundheit der Beschäftigten beim Einrichten und Betreiben von Arbeitsstätten.“



Betriebsverfassungsgesetz (BetrVG)

Arbeitsplatzgestaltung und Mitbestimmung(§90 & §91) 


Arbeitsschutzgesetz – ArbSchG
Arbeitschutz und verbesserungen

Bildschirmarbeitsverordnung – BildscharbV
Gesundheits- und Sicherheitschutz bei der Arbeit an Bildschirmgeräten

Arbeitssicherheitsgesetz
Gesetzte im Bezug auf Fachkräfte von Arbeitssicherheit

Unfallverhütungsvorschriften







https://www.infoquelle.de/job_karriere/arbeitsplatz/arbeitsplatzgestaltung/

### Ergonmie Richtlinien (Bildschirmarbeitsplatz)

Bildschirmarbeitsverordnung – BildscharbV
Regelt den Gesundheits- und Sicherheitschutz bei der Arbeit an Bildschirmgeräten

Es soll den Arbeitnehmer einen Ergonomischen Arbeitsplatz sichern und gewährleisten das die Gesundheit Geschütz wird.


Es wird im bildscharbV nach §5  zum Beispiel geregelt das beim täglichen arbeiten mit Bildschirmgeräten 
zwischen durch andere Beschäftigung ausgeführt werden müssen oder Pause.


Ergonomie = Humanität + Wirtschaftlichkeit


Was könne sie Tun
	Regelmäßige Bildschirmpausen
	60-80 CM abstand zum Monitor
	Tastatur nahe am Körper
	    Tastatur 10 -15 cm von der Tischkante entfernt
	Ausklappen der Hinterfüße (Tastatur)
	DPI-Wert so einstellen das nur weniger Mechanischer aufwand ensteht um diew Maus zu bewegen
	Sitzaltung ändern
    Ergonmoiusche Maus sowie Tastatur
    Handgelenkt nicht abkniken
	
	
	
Was liegt in der Verantwortung des Arbeitggebers um Schutz der Gesdundheit zu gewährleisten
	Keine Störenden Blendung oder Reflektionen
	Beleuchtung der Räumlichkeiten


Ausrichtung des Arbeitssplatz
*Horizontal Standsicher
*Parallel zum Fenster
*Mit Rücken oder gesicht zum Fenster sollte vermieden werden

*Bildschrim min 50 cm von den AUgen entfernt
*Die unterste Bildzeile sollte unterhalb der Augen sein



Arbeitsfläche

Min. Benutz Fläche 1,60m * 1m
Tichhöhe 75 cm
TischFläche 0,8m * 1,6 m
	
https://www.arbeitsschutzgesetz.org/ergonomie-am-arbeitsplatz/

## JON

### Wer trägt die Verantwortung?
Laut § 13 ArbSchG trägt in einem Unternehmen der Geschäftsführer oder ein von 
ihm ernannter Arbeitsschutzbeauftragter die Verantwortung dafür, dass 
(Bildschirm-)Arbeitsplätze sicher sind und geltende Gesetze und Verordnungen 
eingehalten werden.

* https://www.arbeitssicherheit.de/service/lexikon/artikel/verantwortliche-personen.html

### Welche Instanz ist für das Einhalten verantworlich (außerhalb)?
Für das Einhalten der geltenden Gesetze und Verordnungen sind die 
Arbeitsschutzbehörden zuständig. Die zuständige Behörde in Hamburg ist das Amt
für Arbeitsschutz.

* https://www.hamburg.de/arbeitsschutz/wir-ueber-uns/

## Mat

*Stromunfall // Erstehilfe

Schritte nach Arbeitsunfall
1. Untersuchung und Behandlung durch den Durchgangsarzt
	Unternehmen verfügen meist über eine Liste mit entsprechenden Ärzten

Bei einem Stromunfall kann je nach Stromstärke eine leichte bis schwere oder sogar tödliche Verletzung verursacht werden. Am häufigsten betroofen sind Kinder unter sechs Jahren und berufstätige Erwachsene. 
Bei der Ersten Hilfe nach einem Stromunfall gilt: Die eigene Sicherheit steht an erster Stelle! Denn Stromunfälle können für die Ersthelferin/den Ersthelfer schnell lebensbedrohlich werden.
1. Wählen Sie den Notruf 144.
2. Rufen Sie laut um Hilfe und machen Sie umstehende Personen auf die Notfallsituation aufmerksam.
3. Beurteilen Sie die Situation zunächst aus einer Distanz von ungefähr zehn Metern. Befindet sich eine Person im Stromkreis, muss dieser zunächst unterbrochen werden, d.h. den Strom abschalten und sicherstellen, dass der Strom nicht wieder eingeschaltet wird! 

Bei Hochspannungsunfällen muss der Strom durch eine Meldung ans Elektrizitätswerk abgschaltet werden. Auch aus der Ferne kann der Strom auf die Helferin/den Helfer überspringen.
Kann der Strom nicht abgeschaltet werden, kann die Helferin/ der Helfer - jedoch nur, wenn es sich sicher um Niederspannung handelt - versuchen, das Kabel mit nicht leitenden Gegenständen (z.B. trockene Holzstange) von der im Stromkreis befindlichen Person wegzuziehen. Um die/den Verletzte/n berühren und retten zu können, ohne selbst in den Stromkreis zu kommen, ist auf Isolierung zu achten, z.B. dicke Gummihandschuhe anziehen.
4. Wenn die Person aus dem Gefahrenbereich gebracht wurde, überprüfen Sie das Bewusstsein und die Atmung: ansprechen; sanft schütteln; Kopf überstrecken; "hören, sehen, fühlen" für max. zehn Sekunden
5a. Bei normaler Atmung:
Bringen Sie die/den Betroffenen in stabile Seitenlage
Regelmäßige Überprüfung der Atmung bis zum Eintreffen der Rettungskräfte
Decken Sie die/den Betroffenen zu, um eine Unterkühlung zu vermeiden
5b. Bei keiner normalen Atmung:
Starten der Wiederbelebungsmaßnahmen: 20x Herzdruckmassage, 2x beatmen
Wiederholung bis zum Eintreffen der Rettungskräfte oder einem Lebenszeichen des/der Betroffenen. Lassen Sie sich unbedingt helfen und wechseln sich mit einer anderen Helferin/ einem anderen Helfer ab!




### Schritte bei Verletzung während der arbeitszeit

### Stromunfall // Erstehilfe


## Robert
### Durchgangsarzt
Ein Durchgangsartzt auch D-Arzt genannt. Ist ein Facharzt mit dem Schwerpunkt Unfallchirugie.

 

Seine Aufgabe ist es die Patienten zu behandeln, welche Arbeitsunfälle und Wegeunfälle erlitten haben.

 

Außerdem gelten auch Arbeitsunfälle gelten beispielsweise auch Schulunfälle und Unfälle von Helfern im Straßenverkehr.

 

Zusätzlich sind die privaten Pflegepersonen nach dem Pflegeversicherungsgesetz unabhängig vom Alter gesetzlich unfallversichert. Grundsätzlich ist jeder Arbeitnehmer gesetzlich unfallversichert.

 

 

 

A transit doctor, also called a D-doctor. Is a specialist with a focus on accident surgery.

 

His task is to treat the patients who have suffered occupational accidents and commuting accidents.

 

In addition, accidents at work also apply, for example, school accidents and accidents of helpers in road traffic.

 

In addition, private carers are covered by statutory accident insurance under the Care Insurance Act regardless of their age. In principle, every employee is covered by statutory accident insurance.

 
### Piktorgramme SicherheitsKennzeichen
 

2.

 

Piktogramme sind einzelne Symbole/ Icons, welche eine Information durch einfache grafische Darstellung vereinfacht. Dies kann man natürlich auch im Zusammenhang mit Sicherheitskennzeichen verwendet werden.

 

Pictograms are single symbols/icons, which simplify information by simple graphic representation. Of course, this can also be used in connection with safety labels.

 

 

 

 

 

 

 






